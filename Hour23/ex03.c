/* 23L02.c:  Using #ifdef, #ifndef, and #endif */
#include <stdio.h>

#define UPPER_CASE 1
#define LOWER_CASE 0
#define NO_ERROR 0

int main(void) {
#if UPPER_CASE && LOWER_CASE
  printf("Both UPPER_CASE and lower_case is Defined.\n");
#elif UPPER_CASE
  printf("THIS LINE IS PRINTED OUT,\n");
  printf("BECAUSE UPPER_CASE IS DEFINED.\n");
#elif LOWER_CASE
  printf("\nThis line is printed out,\n");
  printf("because LOWER_CASE is not defined.\n");
#else
  printf("This line is printed out,\n");
  printf("because neither UPPER_CASE nor LOWER_CASE is defined.\n");
#endif

  return NO_ERROR;
}