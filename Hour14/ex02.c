/* 14L02.c: Program scope vs block scope */
#include <stdio.h>

int x = 1234;        /* program scope */
double y = 1.234567; /* program scope */

void function_1(int x, double y)
{
    printf("From function_\n  x=%d, y=%f\n", x, y);
}

int main()
{
    int x = 4321; /* block scope 1*/

    function_1(x, y);
    printf("Within the main block:\n  x=%d, y=%f\n", x, y);
    /* a nested block */
    {
        double y = 7.654321; /* block scope 2 */
        function_1(x, y);
        printf("Within the nested block:\n  x=%d, y=%f\n", x, y);
    }
    return 0;
}