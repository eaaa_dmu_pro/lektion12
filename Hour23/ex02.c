#include <stdio.h>

#define MULTIPLY(val1, val2) ((val1) * (val2))

int main() {
  int result;
  result = MULTIPLY(2, 3);
  printf("2 * 3 = %d", result);
}