#include <stdio.h>

int main() {
  char str[] = "Disk file I/O is tricky.";
  const char filename[] = "test_21.txt";

  char *cptr = str;

  FILE *fptr = fopen(filename, "w");
  if (fptr != NULL) {
    while (*cptr != '\0') {
        fputc (*cptr, fptr);
        putchar(*cptr);
        cptr++;
    }
  }
}