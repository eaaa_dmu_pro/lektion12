// Given the following,
// An int variable with block scope and temporary storage
// A constant character variable with block scope
// A float local variable with permanent storage
// A register int variable
// A char pointer initialized with a null character
// write declarations for all of them.

int main() {
    int a;
    const char b = 'A';

    {
        static float c; 
    }

    register int d;
    char *e = '\0';
}