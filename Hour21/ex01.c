#include <stdio.h>

int PrintFile(FILE *file);

int main() {
  FILE *fptr;

  char filename[] = "haiku.txt";
  if ((fptr = fopen(filename, "r")) == NULL) {
    printf("Cannot open %s for writing.\n", filename);
    return 0;
  } else {
    int charCount = PrintFile(fptr);
    printf("\nAntallet af bogstaver i %s er %d", filename, charCount);
    fclose(fptr);
  }
  return 1;
}

int PrintFile(FILE *file) {
  int count = 0, c;
  while ((c = fgetc(file)) != EOF) {
    putchar(c);
    count++;
  }
  return count;
}