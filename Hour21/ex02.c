#include <stdio.h>

void getString(char *input);
void getFileName(char *filename);
int writeStringToFile(char *str, FILE *file);

int main() {
  char strToWrite[30], filename[30];
  int res;
  FILE *fptr;
  getString(strToWrite);
  getFileName(filename);
  while ((fptr = fopen(filename, "a")) == NULL) {
    printf("Det lykkes ikke at åbne filen %s. Prøv igen\n", filename);
    getFileName(filename);
  }
  res = writeStringToFile(strToWrite, fptr);
  if (res != EOF) {
      printf("%s skrevet til filen %s", strToWrite, filename);
  }
  fclose(fptr);
}

void getString(char *input) {
  printf("Indtast en linje der skal skrives til en fil\n> ");
  char *cptr = input;
  char c;
  while ((c = getchar()) != '\n') {
      *cptr = c;
      cptr++;
  }
  *cptr = '\n';
}

void getFileName(char *filename) {
  printf("\nIndtast filnavn der skal skrives til\n>");
  scanf("%s", filename);
}

int writeStringToFile(char *str, FILE *file) { 
    return fputs(str, file); 
}